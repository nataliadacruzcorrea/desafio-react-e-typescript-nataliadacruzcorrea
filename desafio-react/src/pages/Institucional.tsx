import React from 'react';

import Header from "../components/header/Header";
import Main from "../components/main/Main";
import Newsletter from "../components/newsletter/Newsletter";
import Footer from "../components/footer/Footer";

const Institucional = () => {
  return (
  <div>
    <Header/>
    <Main/>
    <Newsletter/>
    <Footer/>
  </div>
  )
};

export default Institucional;
