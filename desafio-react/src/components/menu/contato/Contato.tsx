import React from "react";

import CustomForm from "./form/CustomForm";

import styles from "../contato/Contato.module.css";

function Contato() {
    return (
        <div>
            <h2 className={styles["main-contato-title"]}>PREENCHA O FORMULÁRIO </h2>
                                <CustomForm/>
        </div>  

    )
}

export default Contato;